package org;

import java.io.*;
import java.time.LocalDateTime;

public class LogGenerator {

    private static String logs;
    public static void saveLog(String action, String message){
        logs = LocalDateTime.now() + "                     " + action + "                     <"  + message + ">";
        writeLog();
    }

    public static void saveLog(String action){
        logs = LocalDateTime.now() + "                      " + action + "                     success                     ";
        writeLog();
    }

    public static void saveLog(String action,  Exception e){
        logs = LocalDateTime.now().toString() + "                      " +
                action + "                     failed <"  + e + ">                     " +
                " " + e.getStackTrace()[2].getLineNumber();
        writeLog();
    }

    public static void writeLog(){
        File file;
        DataOutputStream dataOutputStream;
        try {
            String path = "src/org/model/assets/data/";
            file = new File(path + "/logs");
            if (!file.isDirectory()) {
                file.mkdirs();
            }
            file = new File(path+"/logs/log.log");
            dataOutputStream = new DataOutputStream(new BufferedOutputStream(new FileOutputStream(file)));
            dataOutputStream.writeBytes(logs);
            dataOutputStream.close();
        } catch (Exception e){
            e.printStackTrace();
        }
    }
}
