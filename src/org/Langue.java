package org;

import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;

import java.io.Serializable;
import java.util.*;

public class Langue implements Serializable {

    private Locale locale;
    private ResourceBundle bundle;
    private static String langue = "fr";
    private String langueSave="fr";

    public void loadLang(){
        /*
        * Charge la langue et l'applique à changeText*/
        locale = new Locale(langue);
        bundle = ResourceBundle.getBundle("org.model.assets.data.langue.lang", locale);
    }

    public void changeText(Label elmnt, String reference_elmt){
        loadLang();
        elmnt.setText(bundle.getString(reference_elmt));
    }

    public String changeText(String elmnt, String reference_elmt){
        loadLang();
        return bundle.getString(reference_elmt);
    }

    public void changeText(Button elmnt, String reference_elmt){
        loadLang();
        elmnt.setText(bundle.getString(reference_elmt));
    }

    public void changeText(TextField elmnt, String reference_elmt){
        loadLang();
        elmnt.setPromptText(bundle.getString(reference_elmt));
    }

    public void changeText(PasswordField elmnt, String reference_elmt){
        loadLang();
        elmnt.setPromptText(bundle.getString(reference_elmt));
    }

    public static String getLangue() {
        return langue;
    }

    public static void setLangue(String langue) {
        Langue.langue = langue;
    }

    public String getLangueSave () {
        return langueSave;
    }

    public void setLangueSave (String langueSave) {
        this.langueSave = langueSave;
    }
}
