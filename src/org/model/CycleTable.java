package org.model;

import java.sql.SQLException;

public class CycleTable extends InterActionBD {

    private int id, num;
    private String nom;

    public CycleTable () {
    }

    public CycleTable (int num, String nom) {
        this.num = num;
        this.nom = nom;
    }


    public boolean add (String newNom) {
        try {
            requete("INSERT INTO cycle (cycle_nom) VALUES(?);");
            super.getPreparedStatement().setString(1, newNom);
            super.getPreparedStatement().execute();
            return true;
        }catch (Exception e){
            e.printStackTrace();
            return false;
        }
    }


    public boolean update (String newNom) {
        try {
            requete("UPDATE etudiant SET cycle_nom=?;");
            super.getPreparedStatement().setString(1, newNom);
            super.getPreparedStatement().executeUpdate();
            return true;
        }catch (Exception e){
            e.printStackTrace();
            return false;
        }
    }


    public boolean remove(){
        try {
            requete("DELETE FROM cycle WHERE id_cycle=?;");
            super.getPreparedStatement().setInt(1, id);
            super.getPreparedStatement().execute();
            return true;
        }catch (SQLException e){
            return  false;
        }
    }


    public int getNum () {
        return num;
    }

    public void setNum (int num) {
        this.num = num;
    }

    public int getId () {
        return id;
    }

    public void setId (int id) {
        this.id = id;
    }

    public String getNom () {
        return nom;
    }

    public void setNom (String nom) {
        this.nom = nom;
    }





    @Override
    public void info () throws SQLException {

    }
}
