package org.model;

import java.sql.SQLException;

public class ClasseTable extends InterActionBD {

    private int id, num, effectif, id_cycle;
    private String nom;

    public ClasseTable () {
    }

    public ClasseTable (int num, String nom, int effectif) {
        this.num = num;
        this.nom = nom;
        this.effectif = effectif;
    }


    public boolean add (String newNom, int newId_classe) {
        try {
            requete("INSERT INTO classe (classe_nom, classe.id_classe) VALUES(?, ?);");
            super.getPreparedStatement().setString(1, newNom);
            super.getPreparedStatement().setInt(2, newId_classe);
            super.getPreparedStatement().execute();
            return true;
        }catch (Exception e){
            e.printStackTrace();
            return false;
        }
    }


    public boolean update (String newNom, String newEffectif) {
        try {
            requete("UPDATE classe SET classe_nom=?;");
            super.getPreparedStatement().setString(1, newNom);
            super.getPreparedStatement().executeUpdate();
            return true;
        }catch (Exception e){
            e.printStackTrace();
            return false;
        }
    }


    public boolean remove(){
        try {
            requete("DELETE FROM classe WHERE id_classe=?;");
            super.getPreparedStatement().setInt(1, id);
            super.getPreparedStatement().execute();
            return true;
        }catch (SQLException e){
            return  false;
        }
    }


    public int getNum () {
        return num;
    }

    public void setNum (int num) {
        this.num = num;
    }

    public int getId () {
        return id;
    }

    public void setId (int id) {
        this.id = id;
    }

    public String getNom () {
        return nom;
    }

    public void setNom (String nom) {
        this.nom = nom;
    }

    public int getEffectif () {
        return effectif;
    }

    public void setEffectif (int effectif) {
        this.effectif = effectif;
    }

    @Override
    public void info () throws SQLException {

    }
}
