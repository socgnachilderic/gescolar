package org.model;

import java.sql.Date;
import java.sql.SQLException;

public class StudentTable extends InterActionBD {

    private int id, tel, num;
    private Date date_naiss;
    private String nom, prenom, cni, lieu_naiss, email, sexe, ville, pp, pays, matricule, password, username, fax;

    public StudentTable () {
    }

    public StudentTable (int num, String nom, String prenom) {
        this.num = num;
        this.nom = nom;
        this.prenom = prenom;
    }


    public boolean add (String newNom, String newPrenom, String newCni, Date newDateNaiss, String newLieuNaiss,
                           String newEmail, int newTel, String newSexe, String newVille, String newPP, String newFax,
                           String newPays, String newMatricule) {
        try {
            requete("INSERT INTO etudiant (etudiant_nom,etudiant_prenom,etudiant_cni,etudiant_date_naiss,etudiant_lieu_naiss," +
                    "etudiant_email,etudiant_tel,etudiant_sexe,etudiant_ville,etudiant_pp,etudiant_fax,etudiant_pays," +
                    "etudiant_matricule, id_ecole) VALUES(?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?);");
            super.getPreparedStatement().setString(1, newNom);
            super.getPreparedStatement().setString(2, newPrenom);
            super.getPreparedStatement().setString(3, newCni);
            super.getPreparedStatement().setDate(4, newDateNaiss);
            super.getPreparedStatement().setString(5, newLieuNaiss);
            super.getPreparedStatement().setString(6, newEmail);
            super.getPreparedStatement().setInt(7, newTel);
            super.getPreparedStatement().setString(8, newSexe);
            super.getPreparedStatement().setString(9, newVille);
            super.getPreparedStatement().setString(10, newPP);
            super.getPreparedStatement().setString(11, newFax);
            super.getPreparedStatement().setString(12, newPays);
            super.getPreparedStatement().setString(13, newMatricule);
            super.getPreparedStatement().setInt(14, 1);
            super.getPreparedStatement().execute();
            return true;
        }catch (Exception e){
            e.printStackTrace();
            return false;
        }
    }


    public boolean update (String newNom, String newPrenom, String newCni, Date newDateNaiss, String newLieuNaiss,
                           String newEmail, int newTel, String newSexe, String newVille, String newPP, String newFax,
                           String newPays, String newMatricule) {
        try {
            requete("UPDATE etudiant SET etudiant_nom=?,etudiant_prenom=?,etudiant_cni=?,etudiant_date_naiss=?,etudiant_lieu_naiss=?," +
                    "etudiant_email=?,etudiant_tel=?,etudiant_sexe=?,etudiant_ville=?,etudiant_pp=?,etudiant_fax=?,etudiant_pays=?," +
                    "etudiant_matricule=? WHERE id_etudiant=?;");
            super.getPreparedStatement().setString(1, newNom);
            super.getPreparedStatement().setString(2, newPrenom);
            super.getPreparedStatement().setString(3, newCni);
            super.getPreparedStatement().setDate(4, newDateNaiss);
            super.getPreparedStatement().setString(5, newLieuNaiss);
            super.getPreparedStatement().setString(6, newEmail);
            super.getPreparedStatement().setInt(7, newTel);
            super.getPreparedStatement().setString(8, newSexe);
            super.getPreparedStatement().setString(9, newVille);
            super.getPreparedStatement().setString(10, newPP);
            super.getPreparedStatement().setString(11, newFax);
            super.getPreparedStatement().setString(12, newPays);
            super.getPreparedStatement().setString(13, newMatricule);
            super.getPreparedStatement().setInt(14, id);
            super.getPreparedStatement().executeUpdate();
            return true;
        }catch (Exception e){
            e.printStackTrace();
            return false;
        }
    }


    public boolean remove(){
        try {
            requete("DELETE FROM etudiant WHERE id_etudiant=?;");
            super.getPreparedStatement().setInt(1, id);
            super.getPreparedStatement().execute();
            return true;
        }catch (SQLException e){
            return  false;
        }
    }


    public int getNum () {
        return num;
    }

    public void setNum (int num) {
        this.num = num;
    }

    public int getId () {
        return id;
    }

    public void setId (int id) {
        this.id = id;
    }

    public String getFax () {
        return fax;
    }

    public void setFax (String fax) {
        this.fax = fax;
    }

    public String getNom () {
        return nom;
    }

    public void setNom (String nom) {
        this.nom = nom;
    }

    public String getPrenom () {
        return prenom;
    }

    public void setPrenom (String prenom) {
        this.prenom = prenom;
    }

    public String getCni () {
        return cni;
    }

    public void setCni (String cni) {
        this.cni = cni;
    }

    public Date getDate_naiss () {
        return date_naiss;
    }

    public void setDate_naiss (Date date_naiss) {
        this.date_naiss = date_naiss;
    }

    public String getLieu_naiss () {
        return lieu_naiss;
    }

    public void setLieu_naiss (String lieu_naiss) {
        this.lieu_naiss = lieu_naiss;
    }

    public String getEmail () {
        return email;
    }

    public void setEmail (String email) {
        this.email = email;
    }

    public int getTel () {
        return tel;
    }

    public void setTel (int tel) {
        this.tel = tel;
    }

    public String getSexe () {
        return sexe;
    }

    public void setSexe (String sexe) {
        this.sexe = sexe;
    }

    public String getVille () {
        return ville;
    }

    public void setVille (String ville) {
        this.ville = ville;
    }

    public String getPp () {
        return pp;
    }

    public void setPp (String pp) {
        this.pp = pp;
    }

    public String getPays () {
        return pays;
    }

    public void setPays (String pays) {
        this.pays = pays;
    }

    public String getMatricule () {
        return matricule;
    }

    public void setMatricule (String matricule) {
        this.matricule = matricule;
    }

    public String getPassword () {
        return password;
    }

    public void setPassword (String password) {
        this.password = password;
    }

    public String getUsername () {
        return username;
    }

    public void setUsername (String username) {
        this.username = username;
    }

    @Override
    public void info () throws SQLException {

    }
}
