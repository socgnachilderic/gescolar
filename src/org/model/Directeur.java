package org.model;

import java.sql.ResultSet;
import java.sql.SQLException;

public class Directeur extends InterActionBD {

    //private static int id;
    private static String nom, prenom, email, pays, ville;
    private static int tel;

    public Directeur () {
        try {
            info();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void info () throws SQLException {
        requete("SELECT directeur.* FROM directeur INNER JOIN ecole ON directeur.id_directeur = ecole.id_ecole;");
        //super.getPreparedStatement().setInt(1, School.getId_directeur());
        ResultSet resultSet = super.getPreparedStatement().executeQuery();
        while (resultSet.next()){
            nom = resultSet.getString("directeur_nom");
            prenom = resultSet.getString("directeur_prenom");
            email = resultSet.getString("directeur_email");
            tel = resultSet.getInt("directeur_tel");
            pays = resultSet.getString("directeur_pays");
            ville = resultSet.getString("directeur_ville");
        }
    }

    public void update (String nom) throws SQLException {
        requete("UPDATE directeur SET directeur_nom=? WHERE directeur.id_directeur=?;");
        super.getPreparedStatement().setString(1, nom);
        super.getPreparedStatement().setInt(2, School.getId_directeur());
        super.getPreparedStatement().executeUpdate();
        info();
    }
    public void update (String nom, String prenom, String email, int tel, String pays, String ville) throws SQLException {
        requete("UPDATE directeur SET directeur_nom=?, directeur_prenom=?, directeur_email=?, directeur_tel=?, " +
                "directeur_pays=?, directeur_ville=?;");
        super.getPreparedStatement().setString(1, nom);
        super.getPreparedStatement().setString(2, prenom);
        super.getPreparedStatement().setString(3, email);
        super.getPreparedStatement().setInt(4, tel);
        super.getPreparedStatement().setString(5, pays);
        super.getPreparedStatement().setString(6, ville);
        super.getPreparedStatement().executeUpdate();
        info();
    }


    public static String getNom () {
        return nom;
    }

    public static void setNom (String nom) {
        Directeur.nom = nom;
    }

    public static String getPrenom () {
        return prenom;
    }

    public static void setPrenom (String prenom) {
        Directeur.prenom = prenom;
    }

    public static String getEmail () {
        return email;
    }

    public static void setEmail (String email) {
        Directeur.email = email;
    }

    public static String getPays () {
        return pays;
    }

    public static void setPays (String pays) {
        Directeur.pays = pays;
    }

    public static String getVille () {
        return ville;
    }

    public static void setVille (String ville) {
        Directeur.ville = ville;
    }

    public static int getTel () {
        return tel;
    }

    public static void setTel (int tel) {
        Directeur.tel = tel;
    }
}
