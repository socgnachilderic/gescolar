package org.model;

import org.model.utils.ConnexionUtils;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class Admin {

    private static String username, password, ecole, email, nom, prenom, pp;
    private static int telephone, id_ecole=1;
    private static boolean password_visible = false;
    private static String sql;
    private static Connection con = null;

    public static boolean connexion() {
        PreparedStatement preparedStatement = null;
        ResultSet resultSet;

        try {
            con = ConnexionUtils.conDB();
            sql = "SELECT * FROM admin WHERE admin_username = ? and admin_password = ?";
            preparedStatement = con.prepareStatement(sql);
            preparedStatement.setString(1, username);
            preparedStatement.setString(2, password);
            resultSet = preparedStatement.executeQuery();

            if (!resultSet.next()){
                return false;
            }else {
                infoAdmin(resultSet);
                return true;
            }
        }catch (Exception e){
            return false;
        }
    }

    public static boolean update(String newNom, String newPrenom, String newUsername, String newPassword, int newTelephone, String newPp, String newEmail){
        PreparedStatement preparedStatement = null;
        ResultSet resultSet;
        try {
            con = ConnexionUtils.conDB();
            sql= "UPDATE admin SET admin_nom=?, admin_prenom=?, admin_username=?, admin_password=?, admin_telephone=?, " +
                    "admin_pp_admin=?, admin_email=? WHERE admin_username=?";
            preparedStatement = con.prepareStatement(sql);
            preparedStatement.setString(1, newNom);
            preparedStatement.setString(2, newPrenom);
            preparedStatement.setString(3, newUsername);
            preparedStatement.setString(4, newPassword);
            preparedStatement.setInt(5, newTelephone);
            preparedStatement.setString(6, newPp);
            preparedStatement.setString(7, newEmail);
            preparedStatement.setString(8, Admin.getUsername());
            preparedStatement.executeUpdate();
            return true;
        }catch (Exception e){
            System.out.println(" ");
            e.printStackTrace();
            return false;
        }
    }

    public static void infoAdmin(ResultSet resultSet) throws SQLException {
        while (resultSet.next()){
            nom = resultSet.getString("admin_nom");
            prenom = resultSet.getString("admin_prenom");
            id_ecole = resultSet.getInt("admin.id_ecole");
            ecole = School.getNom();
            email = resultSet.getString("admin_email");
            pp = resultSet.getString("admin_pp_admin");
            telephone = resultSet.getInt("admin_telephone");
        }
    }

    public static int getid_ecole () {
        return id_ecole;
    }

    public static void setid_ecole (int id_ecole) {
        Admin.id_ecole = id_ecole;
    }

    public static String getUsername () {
        return username;
    }

    public static void setUsername (String username) {
        Admin.username = username;
    }

    public static String getPassword () {
        return password;
    }

    public static void setPassword (String password) {
        Admin.password = password;
    }

    public static String getEcole () {
        return ecole;
    }

    public static void setEcole (String ecole) {
        Admin.ecole = ecole;
    }

    public static String getEmail () {
        return email;
    }

    public static void setEmail (String email) {
        Admin.email = email;
    }

    public static String getNom () {
        return nom;
    }

    public static void setNom (String nom) {
        Admin.nom = nom;
    }

    public static String getPrenom () {
        return prenom;
    }

    public static void setPrenom (String prenom) {
        Admin.prenom = prenom;
    }

    public static String getPp () {
        return pp;
    }

    public static void setPp (String pp) {
        Admin.pp = pp;
    }

    public static int getTelephone () {
        return telephone;
    }

    public static void setTelephone (int telephone) {
        Admin.telephone = telephone;
    }

    public static boolean isPassword_visible () {
        return password_visible;
    }

    public static void setPassword_visible (boolean password_visible) {
        Admin.password_visible = password_visible;
    }

    public static String getSql () {
        return sql;
    }

    public static void setSql (String sql) {
        Admin.sql = sql;
    }
}
