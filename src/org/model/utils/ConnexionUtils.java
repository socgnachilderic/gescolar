package org.model.utils;

import java.sql.Connection;
import java.sql.DriverManager;

public class ConnexionUtils {

    private Connection conn = null;


    public static Connection conDB(){

        try {
            //Class.forName("com.mysql.jdbc.Driver");
            Connection con = DriverManager.getConnection("jdbc:mysql://localhost:3306/gescolar", "root", "");
            return con;
        }catch (Exception e){
            return null;
        }
    }
}
