package org.model;

import java.sql.SQLException;

public class NiveauTable extends InterActionBD {

    private int id, num, id_cycle;
    private String nom;

    public NiveauTable () {
    }

    public NiveauTable (int num, String nom) {
        this.num = num;
        this.nom = nom;
    }


    public boolean add (String newNom, int id_cycle) {
        try {
            requete("INSERT INTO niveau (niveau_nom, niveau.id_cycle) VALUES(?, ?);");
            super.getPreparedStatement().setString(1, newNom);
            super.getPreparedStatement().setInt(2, id_cycle);
            super.getPreparedStatement().execute();
            return true;
        }catch (Exception e){
            e.printStackTrace();
            return false;
        }
    }


    public boolean update (String newNom) {
        try {
            requete("UPDATE niveau SET niveau_nom=?;");
            super.getPreparedStatement().setString(1, newNom);
            super.getPreparedStatement().executeUpdate();
            return true;
        }catch (Exception e){
            e.printStackTrace();
            return false;
        }
    }


    public boolean remove(){
        try {
            requete("DELETE FROM niveau WHERE id_niveau=?;");
            super.getPreparedStatement().setInt(1, id);
            super.getPreparedStatement().execute();
            return true;
        }catch (SQLException e){
            return  false;
        }
    }


    public int getNum () {
        return num;
    }

    public void setNum (int num) {
        this.num = num;
    }

    public int getId () {
        return id;
    }

    public void setId (int id) {
        this.id = id;
    }

    public String getNom () {
        return nom;
    }

    public void setNom (String nom) {
        this.nom = nom;
    }





    @Override
    public void info () throws SQLException {

    }
}
