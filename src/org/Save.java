package org;

import org.model.AnneeScolaires;

import java.io.*;


/**
 * Classe de gestion des sauvegardes
 * @author SOCGNA KOUYEM Childéric
 * @since 12/05/2019
 * @version 1.0
 * */
public class Save {

    private static String pathSave = "src/org/model/assets/data/saves/";
    private static String pathPersonalFolder;
    private static String anneScolaireFolder;

    public static void write(){
        ObjectOutputStream objectFileSave = null;
        try {
            File file = new File(pathSave);
            if (!file.exists())
                file.mkdirs();

            file = new File(pathSave + "save.skc");
            objectFileSave = new ObjectOutputStream(new BufferedOutputStream(new FileOutputStream(file)));

            MainApp mainAppSave = new MainApp();
            Langue langueSave = new Langue();
            mainAppSave.setNbre_connexionSave(MainApp.getNbre_connexion());
            langueSave.setLangueSave(Langue.getLangue());
            objectFileSave.writeObject(mainAppSave);
            objectFileSave.writeObject(langueSave);
            System.out.println("Sauvegarde réussit avec succès");
        }catch (Exception e){
            e.printStackTrace();
        }finally {
            try {
                objectFileSave.close();
            }catch (Exception e){
                e.printStackTrace();
            }
        }
    }


    public static void read(){
        ObjectInputStream objectFileSave = null;
        File file = null;

        file = new File(pathSave + "save.skc");

        if (file.exists()){
            try {
                objectFileSave = new ObjectInputStream(new BufferedInputStream(new FileInputStream(file)));

                MainApp mainAppRestore = (MainApp) objectFileSave.readObject();
                Langue langueRestore = (Langue) objectFileSave.readObject();
                MainApp.setNbre_connexion(mainAppRestore.getNbre_connexionSave());
                Langue.setLangue(langueRestore.getLangueSave());
                System.out.println(MainApp.getNbre_connexion() + " " + Langue.getLangue());
            }catch (Exception e){
                e.printStackTrace();
            }finally {
                try {
                    objectFileSave.close();
                }catch (IOException e){
                    e.printStackTrace();
                }
            }
        }else
            System.out.println("Aucune sauvegarde");
    }

    public static void personalFolder(){
        if (System.getProperties().toString().contains("Windows"))
            //pathPersonalFolder = "C:/Users/westbook/Documents/GeScolar";
            pathPersonalFolder = "C:/Users/";
        if (System.getProperties().toString().contains("Linux"))
            pathPersonalFolder = "/home/";

        File file = new File(pathPersonalFolder);
        for (String f : file.list()){
            if (!f.contains("efault") && !f.contains("Public") && !f.contains("All Users") && !f.contains(".")) {
                pathPersonalFolder += f + "/Documents/";
                break;
            }
        }
        anneScolaireFolder = (new AnneeScolaires().getDate_debut().toString()) + (new AnneeScolaires().getDate_Fin().toString());
        pathPersonalFolder += "Gescolar/" + anneScolaireFolder + "/";
        file = new File(pathPersonalFolder);
        if (!file.exists()){
            file = new File(pathPersonalFolder + "Students/");
            file.mkdirs();
            file = new File(pathPersonalFolder + "Teachers/");
            file.mkdirs();
            file = new File(pathPersonalFolder + "School/");
            file.mkdirs();
        }


    }


    public static String getPathSave () {
        return pathSave;
    }

    public static void setPathSave (String pathSave) {
        Save.pathSave = pathSave;
    }

    public static String getPathPersonalFolder () {
        return pathPersonalFolder;
    }

    public static void setPathPersonalFolder (String pathPersonalFolder) {
        Save.pathPersonalFolder = pathPersonalFolder;
    }

    public static String getAnneScolaireFolder () {
        return anneScolaireFolder;
    }

    public static void setAnneScolaireFolder (String anneScolaireFolder) {
        Save.anneScolaireFolder = anneScolaireFolder;
    }
}
