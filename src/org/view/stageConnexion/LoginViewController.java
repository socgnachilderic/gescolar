package org.view.stageConnexion;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.*;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import org.*;
import org.model.Admin;
import org.model.Directeur;
import org.model.School;
import org.model.utils.ConnexionUtils;

import java.net.URL;
import java.sql.SQLException;
import java.util.ResourceBundle;

public class LoginViewController implements Initializable, LangueBase {

    private static int nbreConnexionLogin = 1;

    // Widget de langue
    @FXML
    private Label connexion_langue;
    @FXML
    private Button se_connecter_langue;
    @FXML
    private Label text_slogan_langue;
    @FXML
    private Label error_langue;
    @FXML
    private TextField input_username_langue;
    @FXML
    private Label fogert_password_langue;

    // Widget de traitement
    @FXML
    private Button btn_close;
    @FXML
    private ImageView btn_password_off;
    @FXML
    private ImageView btn_password_on;
    @FXML
    private TextField input_password_on;
    @FXML
    private PasswordField input_password_off;
    @FXML
    private ComboBox<String> langue_combobox = new ComboBox<>();

    private ObservableList<String> list_langue;
    private String french = "Français", english = "Anglais";

    @FXML
    public void closeApps(ActionEvent event){
        if (event.getSource() == btn_close)
            System.exit(0);
    }

    @FXML
    public void show_password_on(MouseEvent event){
        if (event.getSource() == btn_password_off){
            change_etat_password(false);
            Admin.setPassword_visible(false);
        }
    }

    @FXML
    public void hide_password_off(MouseEvent event){
        if (event.getSource() == btn_password_on){
            change_etat_password(true);
            Admin.setPassword_visible(true);
        }
    }

    private void change_etat_password(boolean show_password){
        /*@
        * Fonction qui affiche ou masque le champs du mot de passe
        * */
        btn_password_off.setVisible(show_password);
        btn_password_on.setVisible(!show_password);

        input_password_off.setVisible(show_password);
        input_password_on.setVisible(!show_password);

        if (show_password){
            input_password_off.setText(input_password_on.getText());
        } else{
            input_password_on.setText(input_password_off.getText());
        }

    }


    @FXML
    private void connexion(ActionEvent event){
        if (event.getSource() == se_connecter_langue) {
            if (MainApp.getNbre_connexion() == 0){
                logIn();
                MainApp.setNbre_connexion(1);
                Save.write();
                Save.personalFolder();
                Save.read();
            }
            else
                MainApp.sceneApps();
        }
    }

    @FXML
    private void selectLangue(ActionEvent event){
        if (event.getSource() == langue_combobox){
            if (langue_combobox.getValue().equals(french)){
                loadLang("fr");
            }
            if (langue_combobox.getValue().equals(english)){
                loadLang("en");
            }

            Save.write();
            Save.read();
        }
    }


    @Override
    public void initialize(URL location, ResourceBundle resources) {
        loadLang(Langue.getLangue());
        langue_combobox.setItems(list_langue);
    }


    public void loadLang (String lang){
        Langue.setLangue(lang);
        langue.changeText(connexion_langue, "connexion_langue");
        langue.changeText(se_connecter_langue, "se_connecter_langue");
        langue.changeText(text_slogan_langue, "text_slogan_langue");
        langue.changeText(error_langue, "error_langue");
        langue.changeText(input_username_langue, "input_username_langue");
        langue.changeText(input_password_on, "input_password_on");
        langue.changeText(input_password_off, "input_password_off");
        langue.changeText(fogert_password_langue, "fogert_password_langue");
        french = langue.changeText(french, "french");
        english = langue.changeText(english, "english");
        list_langue = FXCollections.observableArrayList(french, english);
        System.out.println(french + " " + english);
    }

    public void logIn(){
        Admin.setUsername(input_username_langue.getText());
        if (Admin.isPassword_visible()){
            Admin.setPassword(input_password_on.getText());
        }else {
            Admin.setPassword(input_password_off.getText());
        }
        System.out.println(Admin.getUsername() + " " + Admin.getPassword());
        if (Admin.connexion()){
            new School();
            new Directeur();
            System.out.println("Ecole " + School.getNom());
            System.out.println("Directeur " + Directeur.getNom());
            MainApp.sceneStage("./view/stageConnexion/firstConnexion/WelcomeView.fxml");
        }else {
            error_langue.setVisible(true);
        }
        System.out.println(Admin.connexion());
    }
}
