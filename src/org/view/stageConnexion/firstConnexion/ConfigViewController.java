package org.view.stageConnexion.firstConnexion;

import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Label;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.scene.image.ImageView;
import javafx.scene.input.KeyEvent;
import javafx.scene.layout.AnchorPane;
import org.Langue;
import org.LangueBase;
import org.model.Admin;
import org.model.School;

import java.net.URL;
import java.util.ResourceBundle;

public class ConfigViewController implements Initializable, LangueBase {

    @FXML
    private AnchorPane sceneConfigPane;
    @FXML
    private Label name_user;
    @FXML
    private Label small_school;
    @FXML
    private Label big_school;
    @FXML
    private Label lien_logo;
    @FXML
    private TextField name_school;
    @FXML
    private TextField name_director;
    @FXML
    private TextField email;
    @FXML
    private TextField tel;
    @FXML
    private TextField address;
    @FXML
    private TextField country;
    @FXML
    private TextField region;
    @FXML
    private TextField ville;
    @FXML
    private ImageView logo;
    @FXML
    private ImageView photo_profil;
    //////////////////////////////////////////////////////////////////////////////
    private static String  name_schoolConfig,  name_directorConifg, emailConfig, addressConfig, countryConfig, regionConfig, villeConfig;
    private static int telConfig;
    @Override
    public void initialize (URL location, ResourceBundle resources) {
        name_user.setText(Admin.getUsername());
        loadLang(Langue.getLangue());
    }

    @Override
    public void loadLang (String lang) {
        Langue.setLangue(lang);
        langue.changeText(small_school, "small_school");
        langue.changeText(big_school, "big_school");
        langue.changeText(lien_logo, "lien_logo");
        langue.changeText(name_school, "name_school");
        langue.changeText(name_director, "name_director");
        langue.changeText(email, "email");
        langue.changeText(tel, "tel");
        langue.changeText(address, "address");
        langue.changeText(country, "country");
        langue.changeText(region, "region");
        langue.changeText(ville, "ville");
    }

    @FXML
    public void remplirChamp(KeyEvent event){
        if (event.getSource() == name_school){
            name_schoolConfig = name_school.getText();
        }
        if (event.getSource() == name_director){
            name_directorConifg = name_director.getText();
        }
        if (event.getSource() == tel){
            telConfig = Integer.parseInt(tel.getText());
        }
        if (event.getSource() == email){
            emailConfig = email.getText();
        }
        if (event.getSource() == address){
            addressConfig = address.getText();
        }
        if (event.getSource() == country){
            countryConfig = country.getText();
        }
        if (event.getSource() == region){
            regionConfig = region.getText();
        }
        if (event.getSource() == ville){
            villeConfig = ville.getText();
        }

    }

    public static boolean successUpdate (){
        System.out.println(name_schoolConfig + " " + name_directorConifg + " " + emailConfig + " " + telConfig + " " + regionConfig + " " + villeConfig);
        School school = new  School();
        return school.update(name_schoolConfig, name_directorConifg, emailConfig, telConfig, addressConfig, regionConfig, villeConfig, countryConfig);
    }
}
