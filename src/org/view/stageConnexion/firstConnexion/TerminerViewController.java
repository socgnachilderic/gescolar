package org.view.stageConnexion.firstConnexion;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.DatePicker;
import javafx.scene.control.Label;
import javafx.scene.image.ImageView;
import javafx.scene.input.KeyEvent;
import javafx.scene.layout.AnchorPane;
import org.Langue;
import org.LangueBase;
import org.model.Admin;
import org.model.AnneeScolaires;

import java.net.URL;
import java.sql.Date;
import java.util.ResourceBundle;

public class TerminerViewController implements Initializable, LangueBase {

    @FXML
    private AnchorPane sceneTerminerPane;
    @FXML
    private Label name_user;
    @FXML
    private ImageView logo_user;
    @FXML
    private Label terminer;
    @FXML
    private Label big_terminer;
    @FXML
    private Label label_msg_terminer;
    @FXML
    private Label date_debut;
    @FXML
    private Label date_fin;
    @FXML
    private DatePicker dateDebut;
    @FXML
    private DatePicker dateFin;
    ////////////////////////////////////////////////////////////
    private static Date dateDebutTerminer, dateFinTerminer;

    @Override
    public void initialize (URL location, ResourceBundle resources) {
        name_user.setText(Admin.getUsername());
        loadLang(Langue.getLangue());
    }

    @Override
    public void loadLang (String lang) {
        Langue.setLangue(lang);
        langue.changeText(terminer, "small_terminer");
        langue.changeText(big_terminer,"big_terminer");
        langue.changeText(label_msg_terminer, "label_msg_terminer");
        langue.changeText(date_debut, "date_debut");
        langue.changeText(date_fin, "date_fin");
    }

    @FXML
    public void remplirChamp(ActionEvent event){
        if (event.getSource() == dateDebut){
            dateDebutTerminer = Date.valueOf(dateDebut.getValue());
        }
        if (event.getSource() == dateFin){
            dateFinTerminer = Date.valueOf(dateDebut.getValue());
        }
    }

    public static boolean successInsert(){
        System.out.println(dateDebutTerminer + " " + dateFinTerminer);
        AnneeScolaires anneeScolaires1 = new AnneeScolaires();
        return anneeScolaires1.insert(dateDebutTerminer, dateFinTerminer);
    }
}
