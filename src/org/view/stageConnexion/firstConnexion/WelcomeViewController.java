package org.view.stageConnexion.firstConnexion;

import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Label;
import javafx.scene.layout.AnchorPane;
import org.Langue;
import org.LangueBase;
import org.MainApp;
import org.model.Admin;

import java.net.URL;
import java.util.ResourceBundle;

public class WelcomeViewController implements Initializable, LangueBase {

    @FXML
    private Label small_welcome;
    @FXML
    private Label big_welcome;
    @FXML
    private Label message_welcome_part1;
    @FXML
    private Label message_welcome_part2;
    @FXML
    private AnchorPane sceneWelcomePane;
    @FXML
    private Label admin_label;


    @Override
    public void initialize(URL location, ResourceBundle resources) {
        //MainApp.getGeneralLayout().getLeft()
        admin_label.setText(Admin.getUsername());
        loadLang(Langue.getLangue());
    }

    @Override
    public void loadLang (String lang) {
        Langue.setLangue(lang);
        langue.changeText(small_welcome, "small_welcome");
        langue.changeText(big_welcome,"big_welcome");
        langue.changeText(message_welcome_part1, "message_welcome_part1");
        langue.changeText(message_welcome_part2, "message_welcome_part2");
    }
}
