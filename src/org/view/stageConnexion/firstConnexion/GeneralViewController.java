package org.view.stageConnexion.firstConnexion;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.Pane;
import org.Langue;
import org.LangueBase;
import org.MainApp;
import org.model.Admin;
import org.model.School;

import java.net.URL;
import java.util.ResourceBundle;

public class GeneralViewController implements Initializable, LangueBase {

    @FXML
    private Button close;
    @FXML
    private Label suivant;
    @FXML
    private ImageView nextIcon;
    @FXML
    private Label configuration;
    @FXML
    private Label bienvenue;
    @FXML
    private Label profil;
    @FXML
    private Label ecole;
    @FXML
    private Label terminer;
    ////////////////////////////////////////////////////////////////////////////////////
    @FXML
    private Pane progression;
    @FXML
    private Pane pane0;
    @FXML
    private Pane pane1;
    @FXML
    private Pane pane2;
    @FXML
    private Pane pane3;
    ////////////////////////////////////////////////////////////////////////////////////


    @FXML
    private void btn_close(ActionEvent event){
        if (event.getSource() == close)
            System.exit(0);
    }

    @FXML
    private void bnt_terminer(ActionEvent event){
        //if (event.getSource() == nextIcon)
    }

    @FXML
    private void avancer(MouseEvent event){
        if (event.getSource() == nextIcon || event.getSource() == suivant) {
            if (MainApp.getGeneralLayout().getCenter().getId().equals("sceneWelcomePane")){
                Admin.connexion();
                MainApp.sceneStage2("./view/stageConnexion/firstConnexion/ProfilView.fxml");
                pane0.setVisible(false);
                pane1.setVisible(true);
            }else if (MainApp.getGeneralLayout().getCenter().getId().equals("sceneProfilPane")){
                if (ProfilViewController.successUpdate()){
                    Admin.setUsername(ProfilViewController.getUsernameProfil());
                    Admin.setPassword(ProfilViewController.getPasswordProfil());
                    Admin.connexion();
                    MainApp.sceneStage2(("./view/stageConnexion/firstConnexion/ConfigView.fxml"));
                    pane0.setVisible(false);
                    pane1.setVisible(false);
                    pane2.setVisible(true);
                }
            }else if (MainApp.getGeneralLayout().getCenter().getId().equals("sceneConfigPane")){
                if (ConfigViewController.successUpdate()){
                    System.out.println("Dans le block");
                    MainApp.sceneStage2(("./view/stageConnexion/firstConnexion/TerminerView.fxml"));
                    pane0.setVisible(false);
                    pane1.setVisible(false);
                    pane2.setVisible(false);
                    pane3.setVisible(true);
                    suivant.setVisible(false);
                }
            }else if (MainApp.getGeneralLayout().getCenter().getId().equals("sceneTerminerPane")){
                if (TerminerViewController.successInsert()){
                    System.out.println("Terminer");
                    MainApp.sceneApps();
                }
            }
        }
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        loadLang(Langue.getLangue());
    }

    @Override
    public void loadLang (String lang) {
        Langue.setLangue(lang);
        langue.changeText(suivant, "suivant");
        langue.changeText(configuration, "configuration");
        langue.changeText(bienvenue, "bienvenue");
        langue.changeText(profil, "profil");
        langue.changeText(ecole, "ecole");
        langue.changeText(terminer, "terminer");
    }


}
