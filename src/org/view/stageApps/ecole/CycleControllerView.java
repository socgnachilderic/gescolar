package org.view.stageApps.ecole;

import de.jensd.fx.glyphs.fontawesome.FontAwesomeIconView;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.input.MouseEvent;
import javafx.scene.paint.Color;
import javafx.scene.shape.Circle;
import org.MainApp;
import org.model.CycleTable;
import org.model.StudentTable;
import org.model.utils.ConnexionUtils;

import java.net.URL;
import java.sql.Connection;
import java.sql.Date;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ResourceBundle;


public class CycleControllerView implements Initializable {


    /////////////////// Gestion des tableaux de cycle ///////////////////////////
    @FXML
    private TableView<CycleTable> cycleTableTableView;
    @FXML
    private TableColumn<CycleTable, String> col_numero;
    @FXML
    private TableColumn<CycleTable, String> col_nom;


    /////////////////// Gestion des infos ///////////////////////////
    @FXML
    private TextField ajoutDeCycle;
    @FXML
    private FontAwesomeIconView deleteIcon;
    @FXML
    private Circle deleteCircle;
    @FXML
    private FontAwesomeIconView updateIcon;
    @FXML
    private Circle updateCircle;
    @FXML
    private FontAwesomeIconView addIcon;
    @FXML
    private Circle addCircle;




    private static ObservableList<CycleTable> cycleTables = FXCollections.observableArrayList();
    private static boolean updateBtn=false;


    @FXML
    public void action(MouseEvent event){
        int selectedIndex = cycleTableTableView.getSelectionModel().getSelectedIndex();
        if ((event.getSource() == deleteIcon || event.getSource() == deleteCircle || event.getSource() == updateIcon
                || event.getSource() == updateCircle) && selectedIndex<0){
            Alert alert = new Alert(Alert.AlertType.WARNING);
            alert.initOwner(MainApp.getStageApps());
            alert.setTitle(" Aucune sélection ");
            alert.setHeaderText("Aucun cycle sélectionné");
            alert.setContentText("S'il vous plaît veuillez sélectionner un cycle");
            alert.showAndWait();

        }else {
            if (event.getSource() == deleteIcon || event.getSource() == deleteCircle){
                cycleTableTableView.getSelectionModel().selectedItemProperty().getValue().remove();
                initialize(null, null);
            }
            if (event.getSource() == updateIcon || event.getSource() == updateCircle){
                updateBtn = !updateBtn;
                if (updateBtn){
                    updateCircle.setFill(Color.web("#ff0012"));
                    //cycleTableTableView.setEditable(true);
                }else{
                    updateCircle.setFill(Color.web("#ffffff"));
                    //cycleTableTableView.setEditable(false);
                    ///cycleTableTableView.getSelectionModel().selectedItemProperty().getValue().update(nomCycleTextField.getText());
                    initialize(null, null);
                }
            }

            if (event.getSource() == addIcon || event.getSource() == addCircle){
                new CycleTable().add(ajoutDeCycle.getText());
                ajoutDeCycle.setText(null);
                initialize(null, null);
            }
        }

    }


    @Override
    public void initialize (URL location, ResourceBundle resources) {
        Connection con = null;
        ResultSet resultSet = null;

        try {
            con = ConnexionUtils.conDB();
            resultSet = con.createStatement().executeQuery("SELECT DISTINCT * FROM cycle ORDER BY cycle_nom;");
            cycleTables.clear();
            int i=0;

            while (resultSet.next()){
                CycleTable cycle = new CycleTable(i+1, resultSet.getString("cycle_nom"));
                cycle.setId(resultSet.getInt("id_cycle"));
                cycleTables.add(cycle);
                i++;
            }
        }catch (SQLException e){
            e.printStackTrace();
        }

        col_numero.setCellValueFactory(new PropertyValueFactory<>("num"));
        col_nom.setCellValueFactory(new PropertyValueFactory<>("nom"));
        cycleTableTableView.setItems(cycleTables);
    }


    public static ObservableList<CycleTable> getCycleTables () {
        return cycleTables;
    }

    public static void setCycleTables (ObservableList<CycleTable> cycleTables) {
        CycleControllerView.cycleTables = cycleTables;
    }

    public TableView<CycleTable> getCycleTableTableView () {
        return cycleTableTableView;
    }

    public void setCycleTableTableView (TableView<CycleTable> cycleTableTableView) {
        this.cycleTableTableView = cycleTableTableView;
    }
}
