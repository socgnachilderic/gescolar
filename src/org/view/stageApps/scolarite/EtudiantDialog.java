package org.view.stageApps.scolarite;

import de.jensd.fx.glyphs.fontawesome.FontAwesomeIconView;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.*;
import org.MainApp;
import org.model.StudentTable;

import java.net.URL;
import java.sql.Date;
import java.util.ResourceBundle;

public class EtudiantDialog implements Initializable {

    @FXML
    private TextField nomEtudiantTextField;
    @FXML
    private TextField prenomEtudiantTextField;
    @FXML
    private TextField matriculeEtudiantTextField;
    @FXML
    private TextField cniEtudiantTextField;
    @FXML
    private DatePicker dateNaissEtudiantTextField;
    @FXML
    private TextField lieuNaissEtudiantTextField;
    @FXML
    private RadioButton sexeEtudiantTextField;
    @FXML
    private TextField paysEtudiantTextField;
    @FXML
    private TextField telEtudiantTextField;
    @FXML
    private TextField emailEtudiantTextField;
    @FXML
    private TextField faxEtudiantTextField;
    @FXML
    private TextField villeEtudiantTextField;
    @FXML
    private ComboBox classeEtudiantLabel;
    @FXML
    private FontAwesomeIconView ppEtudiantLabel;
    @FXML
    private Button addOK;
    @FXML
    private Button closeOK;



    @FXML
    public void action(ActionEvent event){

        if (event.getSource() == closeOK)
            MainApp.getDialogueStage().close();

        if (event.getSource() == addOK){

            new StudentTable().add(nomEtudiantTextField.getText(),
                    prenomEtudiantTextField.getText(),
                    cniEtudiantTextField.getText(),
                    Date.valueOf(dateNaissEtudiantTextField.getValue()),
                    lieuNaissEtudiantTextField.getText(),
                    emailEtudiantTextField.getText(),
                    Integer.parseInt(telEtudiantTextField.getText()),
                    sexeEtudiantTextField.getText(),
                    villeEtudiantTextField.getText(),
                    ppEtudiantLabel.getText(),
                    faxEtudiantTextField.getText(),
                    paysEtudiantTextField.getText(),
                    matriculeEtudiantTextField.getText()
            );
            MainApp.getDialogueStage().close();
        }
    }

    @Override
    public void initialize (URL location, ResourceBundle resources) {

    }
}
