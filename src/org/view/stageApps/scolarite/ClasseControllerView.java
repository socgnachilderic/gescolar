package org.view.stageApps.scolarite;

import de.jensd.fx.glyphs.fontawesome.FontAwesomeIconView;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.input.MouseEvent;
import javafx.scene.paint.Color;
import javafx.scene.shape.Circle;
import org.MainApp;
import org.model.CycleTable;
import org.model.NiveauTable;
import org.model.ClasseTable;
import org.model.utils.ConnexionUtils;

import java.net.URL;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ResourceBundle;

public class ClasseControllerView implements Initializable {
    /////////////////// Gestion des tableaux de classe ///////////////////////////
    @FXML
    private ComboBox<String> niveauCombox;
    @FXML
    private ComboBox<String> cycleCombox;
    @FXML
    private TableView<ClasseTable> classeTableView;
    @FXML
    private TableColumn<ClasseTable, String> col_numero;
    @FXML
    private TableColumn<ClasseTable, String> col_nom;
    @FXML
    private TableColumn<ClasseTable, String> col_effectif;

    /////////////////// Gestion des infos ///////////////////////////
    @FXML
    private TextField ajoutDeClasse;
    @FXML
    private Button matiereButton;
    @FXML
    private FontAwesomeIconView deleteIcon;
    @FXML
    private Circle deleteCircle;
    @FXML
    private FontAwesomeIconView updateIcon;
    @FXML
    private Circle updateCircle;
    @FXML
    private FontAwesomeIconView addIcon;
    @FXML
    private Circle addCircle;

    private static ObservableList<ClasseTable> classeTables = FXCollections.observableArrayList();
    private static boolean updateBtn=false;
    private static int idNiveau;
    private static ClasseTable valueClasse;

    private int idCycle;


    @FXML
    public void action(MouseEvent event){
        int selectedIndex = classeTableView.getSelectionModel().getSelectedIndex();
        if ((event.getSource() == deleteIcon || event.getSource() == deleteCircle || event.getSource() == updateIcon
                || event.getSource() == updateCircle) && selectedIndex<0){
            Alert alert = new Alert(Alert.AlertType.WARNING);
            alert.initOwner(MainApp.getStageApps());
            alert.setTitle(" Aucune sélection ");
            alert.setHeaderText("Aucune classe sélectionné");
            alert.setContentText("S'il vous plaît veuillez sélectionner une classe");
            alert.showAndWait();

        }else {
            if (event.getSource() == deleteIcon || event.getSource() == deleteCircle){
                classeTableView.getSelectionModel().selectedItemProperty().getValue().remove();
                obtenirClasse();
            }
            if (event.getSource() == updateIcon || event.getSource() == updateCircle){
                updateBtn = !updateBtn;
                if (updateBtn){
                    updateCircle.setFill(Color.web("#ff0012"));
                    //classeTableTableView.setEditable(true);
                    //valueClasse = classeTableView.getSelectionModel().selectedItemProperty().getValue();
                    System.out.println(valueClasse.getNom());
                }else{
                    updateCircle.setFill(Color.web("#ffffff"));
                    //classeTableTableView.setEditable(false);
                    ///classeTableTableView.getSelectionModel().selectedItemProperty().getValue().update(nomNiveauTextField.getText());
                    obtenirClasse();
                }
            }

            if (event.getSource() == addIcon || event.getSource() == addCircle){
                new ClasseTable().add(ajoutDeClasse.getText(), idNiveau);
                ajoutDeClasse.setText(null);
                obtenirClasse();
            }

        }

    }

    @FXML
    private void selectCombox(ActionEvent event){
        int selectedIndex = classeTableView.getSelectionModel().getSelectedIndex();
        if (event.getSource() == cycleCombox){
            obtenirNiveau();

        }
        if (event.getSource() == niveauCombox){
            obtenirClasse();
        }

        if ((event.getSource() == matiereButton) && selectedIndex<0){
            Alert alert = new Alert(Alert.AlertType.WARNING);
            alert.initOwner(MainApp.getStageApps());
            alert.setTitle(" Aucune sélection ");
            alert.setHeaderText("Aucune classe sélectionné");
            alert.setContentText("S'il vous plaît veuillez sélectionner une classe");
            alert.showAndWait();

        }else{
            if (event.getSource() == matiereButton){
                valueClasse = classeTableView.getSelectionModel().selectedItemProperty().getValue();
                MainApp.dialoguesStage("./view/stageApps/scolarite/ClasseMatiereDialogView.fxml", "Matières de la classe");
            }
        }

    }


    @Override
    public void initialize (URL location, ResourceBundle resources) {
        obtenirCycle();
        //obtenirNiveau();
    }

    public void obtenirNiveau(){
        Connection con = null;
        ResultSet resultSet = null;
        try {
            con = ConnexionUtils.conDB();
            String sql = "SELECT niveau_nom FROM niveau, cycle WHERE niveau.id_cycle=cycle.id_cycle AND cycle.cycle_nom=? ORDER BY niveau_nom;";
            PreparedStatement preparedStatement = con.prepareStatement(sql);
            preparedStatement.setString(1, cycleCombox.getValue());
            resultSet = preparedStatement.executeQuery();
            ObservableList<String> niveauTable = FXCollections.observableArrayList();
            while (resultSet.next()){
                niveauTable.add(resultSet.getString("niveau_nom"));
            }
            niveauCombox.setItems(niveauTable);
        }catch (SQLException e){
            e.printStackTrace();
        }finally {
            try {
                con.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }

    }

    public void obtenirClasse(){
        System.out.println("yggygu" + niveauCombox.getValue());
        Connection con = null;
        PreparedStatement preparedStatement = null;
        ResultSet resultSet = null;

        try {
            obtenirNiveau();
            con = ConnexionUtils.conDB();
            preparedStatement = con.prepareStatement("SELECT DISTINCT * FROM cycle WHERE cycle.cycle_nom=?;");
            preparedStatement.setString(1, cycleCombox.getValue());
            resultSet = preparedStatement.executeQuery();
            while (resultSet.next()){
                idCycle = resultSet.getInt("id_cycle");
            }
            preparedStatement = con.prepareStatement("SELECT DISTINCT * FROM niveau WHERE niveau.niveau_nom=? AND niveau.id_cycle=?;");
            preparedStatement.setString(1, niveauCombox.getValue());
            preparedStatement.setInt(2,idCycle);
            resultSet = preparedStatement.executeQuery();
            while (resultSet.next()){
                idNiveau = resultSet.getInt("id_niveau");
            }
            preparedStatement = con.prepareStatement("SELECT DISTINCT * FROM classe WHERE classe.id_niveau=?;");
            preparedStatement.setInt(1, idNiveau);
            resultSet = preparedStatement.executeQuery();
            classeTables.clear();
            int i=0;

            while (resultSet.next()){
                ClasseTable classe = new ClasseTable(i+1, resultSet.getString("classe_nom"), resultSet.getInt("classe_effectif"));
                classe.setId(resultSet.getInt("classe.id_classe"));
                classeTables.add(classe);
                i++;
            }
        }catch (SQLException e){
            e.printStackTrace();
        }

        col_numero.setCellValueFactory(new PropertyValueFactory<>("num"));
        col_nom.setCellValueFactory(new PropertyValueFactory<>("nom"));
        col_effectif.setCellValueFactory(new PropertyValueFactory<>("effectif"));
        classeTableView.setItems(classeTables);
        valueClasse = classeTableView.getSelectionModel().selectedItemProperty().getValue();
    }

    public void obtenirCycle(){
        Connection con = null;
        ResultSet resultSet = null;
        try {
            con = ConnexionUtils.conDB();
            resultSet = con.createStatement().executeQuery("SELECT DISTINCT * FROM cycle ORDER BY cycle_nom;");
            int i=0;
            ObservableList<String> cycleTable = FXCollections.observableArrayList();
            while (resultSet.next()){
                CycleTable cycle = new CycleTable(i+1, resultSet.getString("cycle_nom"));
                cycleTable.add(cycle.getNom());
                i++;
            }
            cycleCombox.setItems(cycleTable);
        }catch (SQLException e){
            e.printStackTrace();
        }finally {
            try {
                con.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
    }


    public static ClasseTable getValueClasse () {
        return valueClasse;
    }

    public static void setValueClasse (ClasseTable valueClasse) {
        ClasseControllerView.valueClasse = valueClasse;
    }
}
