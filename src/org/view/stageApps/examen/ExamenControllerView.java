package org.view.stageApps.examen;

import com.jfoenix.controls.JFXComboBox;
import de.jensd.fx.glyphs.fontawesome.FontAwesomeIconView;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.input.MouseEvent;
import javafx.scene.paint.Color;
import javafx.scene.shape.Circle;
import org.MainApp;
import org.model.AnneeScolaires;
import org.model.ExamenTable;
import org.model.utils.ConnexionUtils;

import java.net.URL;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ResourceBundle;
import java.util.concurrent.ExecutionException;

public class ExamenControllerView implements Initializable {

    /////////////////// Gestion des Exmens ///////////////////////////
    @FXML
    private TableView<ExamenTable> examenTableTableView;
    @FXML
    private TableColumn<ExamenTable, String> col_numero;
    @FXML
    private TableColumn<ExamenTable, String> col_nom;
    @FXML
    private TableColumn<ExamenTable, String> col_pourcentage;

    @FXML
    private JFXComboBox<String> periodeComboBox;
    @FXML
    private TextField periodeTextField;
    @FXML
    private FontAwesomeIconView addPeriode;
    @FXML
    private FontAwesomeIconView deletePeriode;


    /////////////////// Gestion des infos ///////////////////////////
    @FXML
    private TextField ajoutDeExamen;
    @FXML
    private TextField ajoutDePourcentage;
    @FXML
    private FontAwesomeIconView deleteIcon;
    @FXML
    private Circle deleteCircle;
    @FXML
    private FontAwesomeIconView updateIcon;
    @FXML
    private Circle updateCircle;
    @FXML
    private FontAwesomeIconView addIcon;
    @FXML
    private Circle addCircle;

    private static boolean updateBtn = false;
    private Connection con = null;
    private PreparedStatement preparedStatement = null;
    private ResultSet resultSet = null;
    private int newId_periode=0;

    @FXML
    public void action(MouseEvent event){
        int selectedIndex=0;
        if ((event.getSource() == deleteIcon || event.getSource() == deleteCircle || event.getSource() == updateIcon
                || event.getSource() == updateCircle) && selectedIndex<0){
            Alert alert = new Alert(Alert.AlertType.WARNING);
            alert.initOwner(MainApp.getStageApps());
            alert.setTitle(" Aucune sélection ");
            alert.setHeaderText("Aucun examen sélectionné");
            alert.setContentText("S'il vous plaît veuillez sélectionner un examen");
            alert.showAndWait();

        }else {
            if (event.getSource() == deleteIcon || event.getSource() == deleteCircle){
                examenTableTableView.getSelectionModel().selectedItemProperty().getValue().remove(newId_periode);
                initialize(null, null);
            }
            if (event.getSource() == updateIcon || event.getSource() == updateCircle){
                updateBtn = !updateBtn;
                if (updateBtn){
                    updateCircle.setFill(Color.web("#ff0012"));
                    //examenTableTableView.setEditable(true);
                }else{
                    updateCircle.setFill(Color.web("#ffffff"));
                    //examenTableTableView.setEditable(false);
                    ///examenTableTableView.getSelectionModel().selectedItemProperty().getValue().update(nomExamenTextField.getText());
                    initialize(null, null);
                }
            }

            /*if (event.getSource() == addIcon || event.getSource() == addCircle){
                ExamenTable examen = new ExamenTable(ajoutDeExamen.getText(), Integer.parseInt(ajoutDePourcentage.getText()));
                examen.add(ajoutDeExamen.getText(), Integer.parseInt(ajoutDePourcentage.getText()));
                try {
                    con = ConnexionUtils.conDB();
                    preparedStatement = con.prepareStatement("SELECT  id_examen FROM examen WHERE examen_nom=? AND  examen_pourcentage=?");
                    preparedStatement.setString(1, ajoutDeExamen.getText());
                    preparedStatement.setInt(2, Integer.parseInt(ajoutDePourcentage.getText()));
                    resultSet = preparedStatement.executeQuery();
                    int newId_examen=0;
                    while (resultSet.next()){
                        newId_examen = resultSet.getInt("id_examen");
                    }
                    preparedStatement = con.prepareStatement("INSERT INTO avoir_examen_periode(id_periode, id_examen) VALUES(?, ?);");
                    preparedStatement.setInt(1, newId_periode);
                    preparedStatement.setInt(2, newId_examen);
                    preparedStatement.executeUpdate();
                    System.out.println("examen " + newId_examen + " periode " + newId_periode);
                }catch (Exception e){
                    e.printStackTrace();
                }
                ajoutDeExamen.setText(null);
                ajoutDePourcentage.setText(null);
                initialize(null, null);
            }*/

            if (event.getSource() == deletePeriode){
                System.out.println("Supprimé");
                try {
                    con = ConnexionUtils.conDB();
                    preparedStatement = con.prepareStatement("DELETE FROM periode_scolaire WHERE periodeScolaire_nom=?");
                    preparedStatement.setString(1, periodeComboBox.getValue());
                    preparedStatement.execute();
                }catch (Exception e){
                    e.printStackTrace();
                }
                initialize(null, null);
            }

            if (event.getSource() == addPeriode){
                System.out.println("Ajouté");
                try {
                    con = ConnexionUtils.conDB();
                    preparedStatement = con.prepareStatement("INSERT INTO periode_scolaire(periodeScolaire_nom, id_anne_scolaire) VALUES(?, ?)");
                    preparedStatement.setString(1, periodeTextField.getText());
                    preparedStatement.setInt(2, 1);
                    preparedStatement.executeUpdate();
                    periodeTextField.setText(null);
                    initialize(null, null);
                }catch (Exception e){
                    e.printStackTrace();
                }
            }
        }

    }

    @FXML
    public void selectCombobox(ActionEvent event){
        if (event.getSource() == periodeComboBox){
            listeExamen();
            try {
                preparedStatement = con.prepareStatement("SELECT  id_periode FROM periode_scolaire WHERE periodeScolaire_nom=?");
                preparedStatement.setString(1, periodeComboBox.getValue());
                resultSet = preparedStatement.executeQuery();
                while (resultSet.next()){
                    newId_periode = resultSet.getInt("id_periode");
                }
            }catch (Exception e){
                e.printStackTrace();
            }
        }
    }

    @Override
    public void initialize (URL location, ResourceBundle resources) {
        listePeriodeScolaire();
    }

    public void listePeriodeScolaire(){
        try {
            System.out.println(periodeComboBox.getValue());
            con = ConnexionUtils.conDB();
            preparedStatement = con.prepareStatement("SELECT * FROM periode_scolaire WHERE id_anne_scolaire=?");
            preparedStatement.setInt(1, 1);
            resultSet = preparedStatement.executeQuery();
            ObservableList<String> periode = FXCollections.observableArrayList();
            while (resultSet.next()){
                periode.add(resultSet.getString("periodeScolaire_nom"));
            }
            periodeComboBox.setItems(periode);
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    public void listeExamen(){
        try {
            ObservableList<ExamenTable> examenTables = FXCollections.observableArrayList();
            con = ConnexionUtils.conDB();
            String sql = "SELECT examen.* FROM examen, periode_scolaire, avoir_examen_periode WHERE " +
                    "avoir_examen_periode.id_examen = examen.id_examen AND " +
                    "avoir_examen_periode.id_periode = periode_scolaire.id_periode AND periode_scolaire.periodeScolaire_nom=?;";
            preparedStatement = con.prepareStatement(sql);
            preparedStatement.setString(1, periodeComboBox.getValue());
            resultSet = preparedStatement.executeQuery();
            examenTables.clear();
            int i=0;

            while (resultSet.next()){
                ExamenTable examen = new ExamenTable(i+1, resultSet.getString("examen_nom"), resultSet.getInt("examen_pourcentage"));
                examen.setId(resultSet.getInt("id_examen"));
                examenTables.add(examen);
                i++;
            }

            col_numero.setCellValueFactory(new PropertyValueFactory<>("num"));
            col_nom.setCellValueFactory(new PropertyValueFactory<>("nom"));
            col_pourcentage.setCellValueFactory(new PropertyValueFactory<>("pourcentage"));
            examenTableTableView.setItems(examenTables);
        }catch (Exception e){
            e.printStackTrace();
        }
    }
}
