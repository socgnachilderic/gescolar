package org.view.stageApps.examen;

import de.jensd.fx.glyphs.fontawesome.FontAwesomeIconView;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.input.MouseEvent;
import javafx.scene.paint.Color;
import javafx.scene.shape.Circle;
import org.MainApp;
import org.model.MatiereTable;
import org.model.utils.ConnexionUtils;

import java.net.URL;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ResourceBundle;


public class MatiereControllerView implements Initializable {


    /////////////////// Gestion des tableaux de matiere ///////////////////////////
    @FXML
    private TableView<MatiereTable> matiereTableTableView;
    @FXML
    private TableColumn<MatiereTable, String> col_numero;
    @FXML
    private TableColumn<MatiereTable, String> col_nom;
    @FXML
    private TableColumn<MatiereTable, String> col_credit;


    /////////////////// Gestion des infos ///////////////////////////
    @FXML
    private TextField ajoutDeMatiere;
    @FXML
    private TextField ajoutDeCredit;
    @FXML
    private FontAwesomeIconView deleteIcon;
    @FXML
    private Circle deleteCircle;
    @FXML
    private FontAwesomeIconView updateIcon;
    @FXML
    private Circle updateCircle;
    @FXML
    private FontAwesomeIconView addIcon;
    @FXML
    private Circle addCircle;




    private static ObservableList<MatiereTable> matiereTables = FXCollections.observableArrayList();
    private static boolean updateBtn=false;


    @FXML
    public void action(MouseEvent event){
        int selectedIndex = matiereTableTableView.getSelectionModel().getSelectedIndex();
        if ((event.getSource() == deleteIcon || event.getSource() == deleteCircle || event.getSource() == updateIcon
                || event.getSource() == updateCircle) && selectedIndex<0){
            Alert alert = new Alert(Alert.AlertType.WARNING);
            alert.initOwner(MainApp.getStageApps());
            alert.setTitle(" Aucune sélection ");
            alert.setHeaderText("Aucun matiere sélectionné");
            alert.setContentText("S'il vous plaît veuillez sélectionner un matiere");
            alert.showAndWait();

        }else {
            if (event.getSource() == deleteIcon || event.getSource() == deleteCircle){
                matiereTableTableView.getSelectionModel().selectedItemProperty().getValue().remove();
                initialize(null, null);
            }
            if (event.getSource() == updateIcon || event.getSource() == updateCircle){
                updateBtn = !updateBtn;
                if (updateBtn){
                    updateCircle.setFill(Color.web("#ff0012"));
                    //matiereTableTableView.setEditable(true);
                }else{
                    updateCircle.setFill(Color.web("#ffffff"));
                    //matiereTableTableView.setEditable(false);
                    ///matiereTableTableView.getSelectionModel().selectedItemProperty().getValue().update(nomMatiereTextField.getText());
                    initialize(null, null);
                }
            }

            if (event.getSource() == addIcon || event.getSource() == addCircle){
                new MatiereTable().add(ajoutDeMatiere.getText(), Integer.parseInt(ajoutDeCredit.getText()));
                ajoutDeMatiere.setText(null);
                ajoutDeCredit.setText(null);
                initialize(null, null);
            }
        }

    }


    @Override
    public void initialize (URL location, ResourceBundle resources) {
        Connection con = null;
        ResultSet resultSet = null;

        try {
            con = ConnexionUtils.conDB();
            resultSet = con.createStatement().executeQuery("SELECT DISTINCT * FROM matiere ORDER BY matiere_nom;");
            matiereTables.clear();
            int i=0;

            while (resultSet.next()){
                MatiereTable matiere = new MatiereTable(i+1, resultSet.getString("matiere_nom"), resultSet.getInt("matiere_credit"));
                matiere.setId(resultSet.getInt("id_matiere"));
                matiereTables.add(matiere);
                i++;
            }
        }catch (SQLException e){
            e.printStackTrace();
        }

        col_numero.setCellValueFactory(new PropertyValueFactory<>("num"));
        col_nom.setCellValueFactory(new PropertyValueFactory<>("nom"));
        col_credit.setCellValueFactory(new PropertyValueFactory<>("credit"));
        matiereTableTableView.setItems(matiereTables);
    }


    public static ObservableList<MatiereTable> getMatiereTables () {
        return matiereTables;
    }

    public static void setMatiereTables (ObservableList<MatiereTable> matiereTables) {
        MatiereControllerView.matiereTables = matiereTables;
    }

    public TableView<MatiereTable> getMatiereTableTableView () {
        return matiereTableTableView;
    }

    public void setMatiereTableTableView (TableView<MatiereTable> matiereTableTableView) {
        this.matiereTableTableView = matiereTableTableView;
    }
}
