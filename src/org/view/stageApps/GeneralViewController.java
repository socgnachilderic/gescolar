package org.view.stageApps;

import de.jensd.fx.glyphs.fontawesome.FontAwesomeIconView;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Label;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.Pane;
import javafx.scene.shape.Circle;
import org.MainApp;

import java.net.URL;
import java.util.ResourceBundle;

public class GeneralViewController implements Initializable {

    //////////////////////////////////// Accueil ////////////////////////////////////
    //=============== Notification ===============//
    @FXML
    private ImageView AccueilNotifImg;
    @FXML
    private Pane AccueilNotifPane;
    @FXML
    private Label AccueilNotifLabel;
    @FXML
    private Circle AccueilNotifCircle;

    //=============== Note ===============//
    @FXML
    private ImageView AccueilNoteImg;
    @FXML
    private Pane AccueilNotePane;
    @FXML
    private Label AccueilNoteLabel;
    @FXML
    private Circle AccueilNoteCircle;

    //=============== Abscence ===============//
    @FXML
    private ImageView AccueilAbsImg;
    @FXML
    private Pane AccueilAbsPane;
    @FXML
    private Label AccueilAbsLabel;
    @FXML
    private Circle AccueilAbsCircle;

    //=============== Resultat ===============//
    @FXML
    private ImageView AccueilResultImg;
    @FXML
    private Pane AccueilResultPane;
    @FXML
    private Label AccueilResultLabel;
    @FXML
    private Circle AccueilResulCircle;

    //=============== Bilan ===============//
    @FXML
    private ImageView AccueilBilanImg;
    @FXML
    private Pane AccueilBilanPane;
    @FXML
    private Label AccueilBilanLabel;
    @FXML
    private Circle AccueilBilanCircle;



    //////////////////////////////////// Scolarité ////////////////////////////////////
    //=============== Niveaux ===============//
    @FXML
    private ImageView ScolariteNivImg;
    @FXML
    private Pane ScolariteNivPane;
    @FXML
    private Label ScolariteNivLabel;
    @FXML
    private Circle ScolariteNivCircle;

    //=============== Classes ===============//
    @FXML
    private FontAwesomeIconView ScolariteClasseImg;
    @FXML
    private Pane ScolariteClassePane;
    @FXML
    private Label ScolariteClasseLabel;
    @FXML
    private Circle ScolariteClasseCircle;

    //=============== Etudiant ===============//
    @FXML
    private ImageView ScolariteEtudiantImg;
    @FXML
    private Pane ScolariteEtudiantPane;
    @FXML
    private Label ScolariteEtudiantLabel;
    @FXML
    private Circle ScolariteEtudiantCircle;



    //////////////////////////////////// Examen ////////////////////////////////////
    //=============== Matieres ===============//
    @FXML
    private FontAwesomeIconView ExamenMatImg;
    @FXML
    private Pane ExamenMatPane;
    @FXML
    private Label ExamenMatLabel;
    @FXML
    private Circle ExamenMatCircle;

    //=============== Examens ===============//
    @FXML
    private FontAwesomeIconView ExamenExaImg;
    @FXML
    private Pane ExamenExaPane;
    @FXML
    private Label ExamenExafLabel;
    @FXML
    private Circle ExamenExaCircle;

    //=============== Plannings ===============//
    @FXML
    private FontAwesomeIconView ExamenPlanniImg;
    @FXML
    private Pane ExamenPlanniPane;
    @FXML
    private Label ExamenPlanniLabel;
    @FXML
    private Circle ExamenPlanniCircle;



    //////////////////////////////////// Ecole ////////////////////////////////////
    //=============== Ecole ===============//
    @FXML
    private FontAwesomeIconView EcoleEcoleImg;
    @FXML
    private Pane EcoleEcolePane;
    @FXML
    private Label EcoleEcoleLabel;
    @FXML
    private Circle EcoleEcoleCircle;

    //=============== Cycle ===============//
    @FXML
    private ImageView EcoleCycleImg;
    @FXML
    private Pane EcoleCyclePane;
    @FXML
    private Label EcoleCycleLabel;
    @FXML
    private Circle EcoleCycleCircle;

    //=============== Enseignants ===============//
    @FXML
    private ImageView EcoleProfImg;
    @FXML
    private Pane EcoleProfPane;
    @FXML
    private Label EcoleProfLabel;
    @FXML
    private Circle EcoleProfCircle;

    //=============== Salles ===============//
    @FXML
    private ImageView EcoleSalleImg;
    @FXML
    private Pane EcoleSallePane;
    @FXML
    private Label EcoleSalleLabel;
    @FXML
    private Circle EcoleSalleCircle;



    //////////////////////////////////// Paramètres ////////////////////////////////////
    //=============== Utilisateurs ===============//
    @FXML
    private FontAwesomeIconView ParamUserImg;
    @FXML
    private Pane ParamUserPane;
    @FXML
    private Label ParamUserLabel;
    @FXML
    private Circle ParamUserCircle;

    //=============== Accessoires ===============//
    @FXML
    private FontAwesomeIconView ParamAccImg;
    @FXML
    private Pane ParamAccPane;
    @FXML
    private Label ParamAccLabel;
    @FXML
    private Circle ParamAccCircle;

    //=============== Paramètres ===============//
    @FXML
    private FontAwesomeIconView ParamRegImg;
    @FXML
    private Pane ParamRegPane;
    @FXML
    private Label ParamRegLabel;
    @FXML
    private Circle ParamRegCircle;

    //=============== Aides ===============//
    @FXML
    private FontAwesomeIconView ParamAideImg;
    @FXML
    private Pane ParamAidePane;
    @FXML
    private Label ParamAideLabel;
    @FXML
    private Circle ParamAideCircle;

    //=============== A propos ===============//
    @FXML
    private FontAwesomeIconView ParamProposImg;
    @FXML
    private Pane ParamProposPane;
    @FXML
    private Label ParamProposLabel;
    @FXML
    private Circle ParamProposCircle;


    @Override
    public void initialize (URL location, ResourceBundle resources) {
        //MainApp.sceneStage("./view/stageApps/accueil/NotificationsView.fxml");
    }


    @FXML
    public void accueil(MouseEvent event){
        if (event.getSource() == AccueilAbsImg || event.getSource() == AccueilAbsCircle
                || event.getSource() == AccueilAbsLabel || event.getSource() == AccueilAbsPane){

            System.out.println("AccueilAbs");
        }

        if (event.getSource() == AccueilNotifImg || event.getSource() == AccueilNotifCircle
                || event.getSource() == AccueilNotifLabel || event.getSource() == AccueilNotifPane){

            System.out.println("AccueilNotif");
        }

        if (event.getSource() == AccueilNoteImg || event.getSource() == AccueilNoteCircle
                || event.getSource() == AccueilNoteLabel || event.getSource() == AccueilNotePane){

            System.out.println("AccueilNote");
            MainApp.sceneStage2("./view/stageApps/accueil/NoteView.fxml");
        }

        if (event.getSource() == AccueilResulCircle || event.getSource() == AccueilResultImg
                || event.getSource() == AccueilResultLabel || event.getSource() == AccueilResultPane){

            System.out.println("AccueilResult");
        }

        if (event.getSource() == AccueilBilanImg || event.getSource() == AccueilBilanCircle
                || event.getSource() == AccueilBilanLabel || event.getSource() == AccueilBilanPane){

            System.out.println("AccueilBilan");
        }
    }


    @FXML
    public void scolarite(MouseEvent event){
        if (event.getSource() == ScolariteNivImg || event.getSource() == ScolariteNivCircle
                || event.getSource() == ScolariteNivLabel || event.getSource() == ScolariteNivPane){

            MainApp.sceneStage2("./view/stageApps/scolarite/NiveauxView.fxml");
        }

        if (event.getSource() == ScolariteEtudiantImg || event.getSource() == ScolariteEtudiantCircle
                || event.getSource() == ScolariteEtudiantLabel || event.getSource() == ScolariteEtudiantPane){

            MainApp.sceneStage2("./view/stageApps/scolarite/EtudiantInfosView.fxml");
        }

        if (event.getSource() == ScolariteClasseImg || event.getSource() == ScolariteClasseCircle
                || event.getSource() == ScolariteClasseLabel || event.getSource() == ScolariteClassePane){

            MainApp.sceneStage2("./view/stageApps/scolarite/ClasseView.fxml");
        }
    }

    @FXML
    public void examen(MouseEvent event){
        if (event.getSource() == ExamenMatImg || event.getSource() == ExamenMatCircle
                || event.getSource() == ExamenMatLabel || event.getSource() == ExamenMatPane){

            MainApp.sceneStage2("./view/stageApps/examen/MatiereView.fxml");
        }

        if (event.getSource() == ExamenExaImg || event.getSource() == ExamenExaCircle
                || event.getSource() == ExamenExafLabel || event.getSource() == ExamenExaPane){

            MainApp.sceneStage2("./view/stageApps/examen/ExamenView.fxml");
        }

        if (event.getSource() == ExamenPlanniImg || event.getSource() == ExamenPlanniCircle
                || event.getSource() == ExamenPlanniLabel || event.getSource() == ExamenPlanniPane){

            System.out.println("ExamenPlanni");
        }
    }

    @FXML
    public void ecoles(MouseEvent event){
        if (event.getSource() == EcoleEcoleImg || event.getSource() == EcoleEcoleCircle
                || event.getSource() == EcoleEcolePane || event.getSource() == EcoleEcoleLabel){

            System.out.println("EcoleEcole");
            MainApp.sceneStage2("./view/stageApps/ecole/EcoleView.fxml");
        }

        if (event.getSource() == EcoleCycleImg || event.getSource() == EcoleCycleCircle
                || event.getSource() == EcoleCycleLabel || event.getSource() == EcoleCyclePane){

            System.out.println("EcoleCycle");
            MainApp.sceneStage2("./view/stageApps/ecole/CycleView.fxml");
        }

        if (event.getSource() == EcoleProfImg || event.getSource() == EcoleProfCircle
                || event.getSource() == EcoleProfLabel || event.getSource() == EcoleProfPane){

            System.out.println("EcoleProf");
            MainApp.sceneStage2("./view/stageApps/ecole/ProfesseurView.fxml");
        }

        if (event.getSource() == EcoleSalleImg || event.getSource() == EcoleSalleCircle
                || event.getSource() == EcoleSalleLabel || event.getSource() == EcoleSallePane){

            System.out.println("EcoleSalle");
            MainApp.sceneStage2("./view/stageApps/ecole/SalleView.fxml");
        }
    }

    @FXML
    public void parametre(MouseEvent event){
        if (event.getSource() == ParamUserImg || event.getSource() == ParamUserCircle
                || event.getSource() == ParamUserLabel || event.getSource() == ParamUserPane){

            System.out.println("ParamUser");
        }

        if (event.getSource() == ParamAccImg || event.getSource() == ParamAccCircle
                || event.getSource() == ParamAccLabel || event.getSource() == ParamAccPane){

            System.out.println("ParamAcc");
        }

        if (event.getSource() == ParamRegImg || event.getSource() == ParamRegCircle
                || event.getSource() == ParamRegLabel || event.getSource() == ParamRegPane){

            System.out.println("ParamReg");
        }

        if (event.getSource() == ParamAideImg || event.getSource() == ParamAideCircle
                || event.getSource() == ParamAideLabel || event.getSource() == ParamAidePane){

            System.out.println("ParamAide");
        }

        if (event.getSource() == ParamProposImg || event.getSource() == ParamProposCircle
                || event.getSource() == ParamProposLabel || event.getSource() == ParamProposPane){

            System.out.println("ParamPropos");
        }
    }
}
